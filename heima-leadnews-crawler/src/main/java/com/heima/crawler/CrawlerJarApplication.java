package com.heima.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 爬虫服务
 */
@SpringBootApplication
public class CrawlerJarApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrawlerJarApplication.class,args);
    }
}
